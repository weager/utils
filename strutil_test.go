package utils

import (
	"testing"
)

type equalTest struct {
	a []string
	b []string
	r bool
}

var equalTests = []equalTest{
	{nil, nil, true},
	{[]string{""}, []string{""}, true},
	{[]string{"1"}, []string{"1"}, true},
	{[]string{"", "1"}, []string{"", "1"}, true},
	{[]string{"1", "2"}, []string{"1", "2"}, true},
}

func runEqualTest(t *testing.T, f func(a, b []string) bool, testCases []equalTest) {
	for _, tc := range testCases {
		if tc.r != f(tc.a, tc.b) {
			t.Errorf("expect[%s], but[%s]", tc.b, tc.a)
		}
	}
}

func TestEqual(t *testing.T) {
	runEqualTest(t, Equal, equalTests)
}

type splitTest struct {
	s  string
	ss []string
	r  bool
}

var splitBlankTests = []splitTest{
	{"", nil, true},
	{" ", nil, true},
	{" 1 ", []string{"1"}, true},
	{" 1", []string{"1"}, true},
	{"1 ", []string{"1"}, true},
	{"12", []string{"12"}, true},
	{"1 2", []string{"1", "2"}, true},
	{" 1 2 ", []string{"1", "2"}, true},
	{" 1   2 ", []string{"1", "2"}, true},
	{"  1   2  ", []string{"1", "2"}, true},
}

func runSplitBlankTest(t *testing.T, split func(s string, spliter string) []string, testCases []splitTest) {
	for _, tc := range testCases {
		strArr := Split(tc.s, " ")
		if tc.r != Equal(strArr, tc.ss) {
			t.Errorf("expect[%s], but[%s]", tc.ss, strArr)
		}
	}
}

func TestSplit(t *testing.T) {
	runSplitBlankTest(t, Split, splitBlankTests)
}

type subTest struct {
	s              string
	offset, length int
	sub            string
	r              bool
}

var subTests = []subTest{
	{"01234", -2, 0, "", true},
	{"01234", 2, 2, "23", true},
	{"01234", 2, 5, "23", true},
	{"01234", 2, 5, "234", true},
}

func runSubTest(t *testing.T, f func(s string, offset, lenght int) string, testCases []subTest) {
	for _, tc := range testCases {
		subStr := f(tc.s, tc.offset, tc.length)
		if tc.r != (subStr == tc.sub) {
			t.Errorf("expect[%s], but[%s]", tc.sub, subStr)
		}
	}
}
