package utils

import (
	"strings"
)

/*
compare two array, any element with same index is not the same, will return false
*/
func Equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

/*
split a string by a indicated string
for example, split by blank
case1: "  1  2  3   " --> "1","2","3"
case2: "" --> nil
case3: " " --> nil
*/
func Split(s string, sep string) (ss []string) {
	primaryArray := strings.Split(s, sep)
	for _, e := range primaryArray {
		if e != "" {
			ss = append(ss, e)
		}
	}
	return
}

/*
get substring from offset, the length of substring is no more than len(s)-offset+1
case1: "01234", -2, 0 --> ""
case2: "01234", 2, 2 --> "23"
case3: "01234", 2, 5 -->  "23"
case4: "01234", 2, 5 --> "234"
*/
func Sub(s string, offset, length int) string {
	if len(s) == 0 || offset >= len(s) {
		return ""
	}
	if offset < 0 {
		offset = 0
	}
	if length+offset-1 > len(s) {
		length = len(s) - offset + 1
	}
	return s[offset : offset+length-1]
}
