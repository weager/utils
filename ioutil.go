package utils

import (
	"bufio"
)

// read a line even the size of it is bigger then 4096 bytes. 
func ReadLine(bufReader *bufio.Reader) (line string, err error) {
	var tmpBuf []byte
	tmpBuf, isPrefix, err := bufReader.ReadLine()
	if isPrefix {
		for {
			dataSegment, isPrefix, err := bufReader.ReadLine()
			if err == nil {
				tmpBuf = append(tmpBuf, dataSegment...)
			} else {
				break
			}
			if !isPrefix {
				break
			}
		}
	}
	return string(tmpBuf), err
}
